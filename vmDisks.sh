#!/bin/bash
# Script for hot adding / extending disk to Virtual machine
# CHANGELOG
# v0.3 - Fixed console menu. addjusted deviceList function to suite both newDisk and existingDisk
# v0.2 - moved fdisk command to deviceList function.
# v0.1 - initial script
# Created by Mads Boye - mb@madsboye.dk
# TODO - make script runable with arguemnts instead of interactive.
# Check for root
set -e 
sudo -v

#Store executable in variables
FDISK=`which fdisk`
LVS=`which lvs`
PVS=`which pvs`
PVCREATE=`which pvcreate`
PVRESIZE=`which pvresize`
VGCREATE=`which vgcreate`
LVCREATE=`which lvcreate`
LVEXTEND=`which lvextend`
RESIZE2FS=`which resize2fs`
MKEXT4=`which mkfs.ext4`

#Check if required tools are available on the system
if [ -z "$FDISK" ];
	then
	echo "Required tools is not fould on system. Please check that fdisk, e2fsprogs ,and lvm exists, and restart the script..."
fi

#Functions
#Get all devices that starts with /dev/sd
deviceList() {
	grep -oE sd[a-z]*[a-z] /proc/partitions | uniq
}
newDeviceScan(){
	for i in `ls -A1 /sys/class/scsi_host/`; do
		echo "- - -" | sudo tee /sys/class/scsi_host/$i/scan > /dev/null
	done
}
existingDeviceScan(){
	for i in `ls -A1 /sys/class/scsi_device/`; do
		echo 1 | sudo tee /sys/class/scsi_device/$i/device/rescan > /dev/null
	done
}
#function to size of existing drives.
diskSizes=""
deviceSize(){
	SAVEIFS=$IFS
	IFS=$(echo -en "\n\b")
	diskSizes=($(lsblk -b | grep disk | awk '{a[$1]+=$4} END{for(k in a) print k,a[k]}'))
	IFS=$SAVEIFS
}
#newDisk function scans for new disk devices, and compare two arrays. If new disk is added it will return names of new disk(s).
newDisk() {
	devListBefore=($(deviceList))
	echo "scanning for new disk..."
	newDeviceScan
	sleep 1
	devListAfter=($(deviceList))
	if [ "${#devListBefore[@]}" -ne "${#devListAfter[@]}" ]; then
		devDiff=(`echo ${devListAfter[@]} ${devListBefore[@]} | tr ' ' '\n' | sort | uniq -u`)
			echo "New disks are: ${devDiff[@]}"
		else
			echo "No new disks added..."
	fi
}
#existingDisk finds drive which size has been extended.
existingDisk() {
	deviceSize
	deviceSizesBefore=("${diskSizes[@]}")
	echo "rescanning exsting disks, to online added space..."
	existingDeviceScan
	sleep 1
	deviceSize
	deviceSizesAfter=("${diskSizes[@]}")
	arrayLength=${#deviceSizesAfter[@]}


	for i in $(seq 0 $(($arrayLength-1))); do
		if [  "${deviceSizesBefore[$i]}" != "${deviceSizesAfter[$i]}" ]; then
			deviceName=$(echo ${diskSizes[$i]} | awk -F ' ' '{print $1}')
			deviceNewSize=$(lsblk -n -d -o size /dev/$deviceName)
			echo "$deviceName has new size of:$deviceNewSize";
		fi;
	done
	echo "Done..."
}

lvmCreate() {
	echo "Create lvm disk"
	pvcreate /dev/
}

#lvmExtend() {
#
#}

# usage cases
#1) new disk added - scan bus - added to lvm - format and add to /etc/fstab
#2) extend existing disks - scan bus -resize pv - resize lv - resize fs.
#Bash selection menue
clear
echo "This script i used to add new disks and extend extisting disks on VMware virtual machines."
while true
do
PS3='Please enter your choice: '
options=("New disk" "Extend disk" "New disk LVM" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "New disk")
            echo "You have choosen to add a new disk"
            newDisk
	    break
            ;;
        "Extend disk")
            echo "You have choosen to extend an existing disk"
            existingDisk
	    break
            ;;
        "New disk LVM")
            echo "You have choosen to add new disk to lvm"
            newLVM
	    break
            ;;
        "Quit")
	    echo "Thank you... Goodbye!"
	    clear
            exit
            ;;
        *) echo invalid option;;
    esac
done
done

# #List virtuelle scsi_devices
# ll /sys/class/scsi_device/
# #Rescan alle
# for i in `ls -A1 /sys/class/scsi_device/`; do echo 1 > /sys/class/scsi_device/$i/device/rescan; done
# #Check af disk er blevet detekteret af OS som værende større
# fdisk -l
# #Diske med LVM skal resizes - HUSK AT TILRETTE /dev/sd*
# pvresize /dev/sdb
# #Find LVM sti
# root@busdata:~# ll /dev/mapper/
# total 0
# drwxr-xr-x  2 root root     100 Apr 25 09:03 ./
# drwxr-xr-x 17 root root    4220 May 17 06:59 ../
# crw-------  1 root root 10, 236 Apr 25 09:03 control
# lrwxrwxrwx  1 root root       7 May 30 13:08 daisy--template--vg-data -> ../dm-1
# lrwxrwxrwx  1 root root       7 Apr 25 09:03 daisy--template--vg-root -> ../dm-0

# daisy--template--vg-{data,root} er de to virtuelle diske.

# #Forøg disk i LVM
# lvextend -l +100%FREE /dev/daisy-template-vg/data
# #Resize filsystemet på disken
# resize2fs /dev/daisy-template-vg/data
